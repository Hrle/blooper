use std::cmp::Ordering;
use std::sync::{Arc, Mutex};

use cpal::traits::{DeviceTrait, HostTrait, StreamTrait};
use cpal::{
    Device, Host, HostId, InputCallbackInfo, OutputCallbackInfo, StreamConfig, StreamError,
    SupportedBufferSize, SupportedStreamConfigRange,
};

use ringbuf::HeapRb as RingBuf;

use super::{
    config::{AudioConfig, ChannelCount, Sample, PREFERRED_AUDIO_CONFIG},
    AudioContext, AudioResult, LowLevelAudioBackendTrait,
};

struct LowLevelAudioBackendConfig {
    audio: AudioConfig,
    input_device: Device,
    input_config: StreamConfig,
    output_device: Device,
    output_config: StreamConfig,
}

struct LowLevelAudioBackendStreams {
    processor: Arc<dyn Fn(&mut AudioContext) + Send + Sync>,
    input_stream: Box<dyn StreamTrait>,
    output_stream: Box<dyn StreamTrait>,
}

struct LowLevelAudioBackendValue {
    host: Host,
    config: Option<LowLevelAudioBackendConfig>,
    streams: Option<LowLevelAudioBackendStreams>,
}
unsafe impl Send for LowLevelAudioBackendValue {}

pub struct LowLevelAudioBackend {
    value: Arc<Mutex<LowLevelAudioBackendValue>>,
}

impl LowLevelAudioBackendTrait for LowLevelAudioBackend {
    // TODO: handle mutex poisoning
    fn process(&mut self, processor: Arc<dyn Fn(&mut AudioContext) + Send + Sync>) -> AudioResult {
        let mut value = self.value.lock().unwrap();
        value.process(processor)
    }

    // TODO: handle mutex poisoning
    fn config(&mut self, config: AudioConfig) -> AudioResult {
        let mut value = self.value.lock().unwrap();
        value.config(config)
    }
}

impl LowLevelAudioBackend {
    pub fn new() -> Self {
        Self {
            value: Arc::new(Mutex::new(LowLevelAudioBackendValue::new())),
        }
    }
}

impl LowLevelAudioBackendValue {
    fn new() -> Self {
        let host_id: HostId = cpal::available_hosts()
            .iter()
            .max_by(Self::cmp_hosts)
            .expect("no host available")
            .to_owned();
        dbg!(host_id);
        let host: Host = cpal::host_from_id(host_id).expect("failed creating host");

        let mut backend = LowLevelAudioBackendValue {
            host,
            config: None,
            streams: None,
        };
        backend.config(PREFERRED_AUDIO_CONFIG).unwrap();

        backend
    }

    fn process(&mut self, processor: Arc<dyn Fn(&mut AudioContext) + Send + Sync>) -> AudioResult {
        let config = match &self.config {
            Some(config) => config,
            None => {
                self.config(PREFERRED_AUDIO_CONFIG).unwrap();
                &self.config.as_ref().unwrap()
            }
        };

        match &self.streams {
            Some(streams) => {
                streams.input_stream.pause().unwrap();
                streams.output_stream.pause().unwrap();
            }
            None => {}
        };
        self.streams = None;

        let (mut input_producer, mut input_consumer) =
            RingBuf::new((config.audio.buffer_size * 10).try_into().unwrap()).split();
        let mut output_stream_input_storage =
            vec![0.0; config.audio.buffer_size.try_into().unwrap()];

        let input_stream = match config.input_device.build_input_stream(
            &config.input_config.clone(),
            move |data: &[Sample], _info: &InputCallbackInfo| {
                for sample in data {
                    match input_producer.push(*sample) {
                        Ok(_) => {}
                        Err(_) => break,
                    }
                }
            },
            Self::err_fn,
        ) {
            Ok(stream) => stream,
            Err(error) => return Err((config.audio, error.to_string())),
        };

        let output_stream_processor = processor.clone();
        let output_stream = match config.output_device.build_output_stream(
            &config.output_config.clone(),
            move |data: &mut [Sample], _info: &OutputCallbackInfo| {
                input_consumer.pop_slice(output_stream_input_storage.as_mut_slice());
                output_stream_processor(&mut AudioContext {
                    input: output_stream_input_storage.as_slice(),
                    output: data,
                });
            },
            Self::err_fn,
        ) {
            Ok(stream) => stream,
            Err(error) => return Err((config.audio, error.to_string())),
        };

        self.streams = Some(LowLevelAudioBackendStreams {
            processor: processor.clone(),
            input_stream: Box::new(input_stream),
            output_stream: Box::new(output_stream),
        });

        Ok(config.audio)
    }

    fn config(&mut self, config: AudioConfig) -> AudioResult {
        let input_device = self
            .host
            .input_devices()
            .expect("no input devices available")
            .max_by(Self::cmp_devices)
            .expect("no input device available");
        println!(
            "Input device: {}",
            input_device.name().expect("input device has no name")
        );

        let output_device = self
            .host
            .output_devices()
            .expect("no input devices available")
            .max_by(Self::cmp_devices)
            .expect("no input device available");
        println!(
            "Output device: {}",
            input_device.name().expect("input device has no name")
        );

        let (input_config, audio) = LowLevelAudioBackendValue::make_config(&input_device, config);
        let (output_config, ..) = LowLevelAudioBackendValue::make_config(&output_device, config);

        let config = LowLevelAudioBackendConfig {
            audio,
            input_device,
            input_config,
            output_device,
            output_config,
        };

        self.config = Some(config);

        // TODO: fix recursion
        match &self.streams {
            Some(streams) => {
                self.process(streams.processor.clone()).unwrap();
            }
            None => {}
        }

        Ok(audio)
    }

    fn make_config(device: &Device, config: AudioConfig) -> (StreamConfig, AudioConfig) {
        let supported_configs = device
            .supported_input_configs()
            .expect("error while querying input configs");
        let supported_config = supported_configs
            .max_by(Self::make_cmp_supported_streams_config_ranges(config))
            .expect("no supported input config");
        let channels = supported_config.channels();

        let raw_sample_rate = (config.sample_rate).clamp(
            match supported_config.min_sample_rate() {
                cpal::SampleRate(sample_rate) => sample_rate as f32,
            },
            match supported_config.max_sample_rate() {
                cpal::SampleRate(sample_rate) => sample_rate as f32,
            },
        );
        let sample_rate = cpal::SampleRate(raw_sample_rate as u32);
        let supported_config = supported_config.with_sample_rate(sample_rate);
        dbg!(&supported_config);

        let raw_buffer_size = config.buffer_size.clamp(
            match supported_config.buffer_size() {
                cpal::SupportedBufferSize::Unknown => u32::MIN,
                cpal::SupportedBufferSize::Range { min, .. } => min.to_owned(),
            },
            match supported_config.buffer_size() {
                cpal::SupportedBufferSize::Unknown => u32::MAX,
                cpal::SupportedBufferSize::Range { max, .. } => max.to_owned(),
            },
        );
        let buffer_size = cpal::BufferSize::Fixed(raw_buffer_size);
        let mut config: cpal::StreamConfig = supported_config.into();
        config.buffer_size = buffer_size.clone();

        (
            config,
            AudioConfig {
                channels,
                sample_rate: raw_sample_rate,
                buffer_size: raw_buffer_size,
            },
        )
    }

    fn err_fn(err: StreamError) {
        eprintln!(
            "an error occurred on the input or output audio stream: {}",
            err
        )
    }

    fn cmp_hosts<'x, 'y>(x: &'x &HostId, _y: &'y &HostId) -> std::cmp::Ordering {
        match x {
            // NOTE: still waiting for Pipewire to be supported
            // PR: https://github.com/RustAudio/cpal/pull/692
            #[cfg(any(
                target_os = "linux",
                target_os = "dragonfly",
                target_os = "freebsd",
                target_os = "netbsd"
            ))]
            cpal::HostId::Jack => std::cmp::Ordering::Greater,
            _other => std::cmp::Ordering::Less,
        }
    }

    fn cmp_devices<'x, 'y>(_x: &'x Device, _y: &'y Device) -> Ordering {
        Ordering::Less
    }

    fn make_cmp_supported_streams_config_ranges(
        config: AudioConfig,
    ) -> Box<dyn Fn(&SupportedStreamConfigRange, &SupportedStreamConfigRange) -> Ordering> {
        return Box::new(move |x, y| {
            let channels: ChannelCount = x.channels().into();
            if channels == config.channels {
                return match x.sample_format() {
                    cpal::SampleFormat::I16 => std::cmp::Ordering::Less,
                    cpal::SampleFormat::U16 => std::cmp::Ordering::Less,
                    cpal::SampleFormat::F32 => {
                        if match x.buffer_size() {
                            SupportedBufferSize::Unknown => u32::MAX,
                            SupportedBufferSize::Range { min, .. } => min.to_owned(),
                        } <= match y.buffer_size() {
                            SupportedBufferSize::Unknown => u32::MAX,
                            SupportedBufferSize::Range { min, .. } => min.to_owned(),
                        } {
                            std::cmp::Ordering::Greater
                        } else {
                            std::cmp::Ordering::Less
                        }
                    }
                };
            }

            return Ordering::Less;
        });
    }
}
