use std::{cmp::Ordering, sync::Arc};

use portaudio::{
    DeviceIndex, DeviceInfo, Duplex, DuplexStreamCallbackArgs, DuplexStreamSettings, HostApiIndex,
    HostApiInfo, HostApiTypeId, Input, InputStreamCallbackArgs, InputStreamSettings, NonBlocking,
    Output, OutputStreamCallbackArgs, OutputStreamSettings, PortAudio, Stream,
    StreamCallbackResult, StreamParameters,
};

use ringbuf::HeapRb as RingBuf;

use super::{
    r#const::{PREFERRED_BUFFER_SIZE, PREFERRED_CHANNELS, PREFERRED_SAMPLE_RATE},
    AudioConfig, AudioContext, LowLevelAudioBackendTrait,
};

pub struct LowLevelAudioBackend {
    instance: PortAudio,
    device_index: DeviceIndex,
    duplex_stream: Option<Stream<NonBlocking, Duplex<f32, f32>>>,
    input_stream: Option<Stream<NonBlocking, Input<f32>>>,
    output_stream: Option<Stream<NonBlocking, Output<f32>>>,
}

impl LowLevelAudioBackendTrait for LowLevelAudioBackend {
    fn stream(
        &mut self,
        processor: Arc<dyn Fn(&mut AudioContext) + Send + Sync>,
        config: Option<AudioConfig>,
    ) {
        self.pause();
        self.duplex_stream = None;
        self.input_stream = None;
        self.output_stream = None;

        match self.make_duplex_stream(processor.clone()) {
            Ok(duplex_stream) => {
                self.duplex_stream = Some(duplex_stream);
            }
            Err(_) => {
                let (input_stream, output_stream) =
                    match self.make_input_output_streams(processor.clone()) {
                        Ok(streams) => streams,
                        Err(error) => return error.to_string(),
                    };
                self.input_stream = Some(input_stream);
                self.output_stream = Some(output_stream);
            }
        }
    }

    fn play(&mut self) {
        self.duplex_stream.as_mut().map(|stream| {
            stream
                .start()
                .expect("failed starting portaudio duplex stream")
        });

        self.output_stream.as_mut().map(|stream| {
            stream
                .start()
                .expect("failed starting portaudio output stream")
        });

        self.input_stream.as_mut().map(|stream| {
            stream
                .start()
                .expect("failed starting portaudio input stream")
        });
    }

    fn pause(&mut self) {
        self.duplex_stream.as_mut().map(|stream| {
            stream
                .stop()
                .expect("failed stopping portaudio duplex stream")
        });

        self.output_stream.as_mut().map(|stream| {
            stream
                .stop()
                .expect("failed stopping portaudio output stream")
        });

        self.input_stream.as_mut().map(|stream| {
            stream
                .stop()
                .expect("failed stopping portaudio input stream")
        });
    }
}

impl LowLevelAudioBackend {
    pub fn new() -> Box<dyn LowLevelAudioBackendTrait> {
        let instance = portaudio::PortAudio::new().expect("failed initializing portaudio");

        let (host_index, host_info) = instance
            .host_apis()
            .into_iter()
            .max_by(Self::cmp_hosts)
            .expect("failed finding host");
        dbg!(&host_index, &host_info);

        let (device_index, device_info) = instance
            .devices()
            .expect("failed fetching portaudio devices")
            .into_iter()
            .map_while(|result| match result {
                Ok(device) => Some(device),
                Err(_) => None,
            })
            .max_by(Self::make_cmp_devices(host_index, host_info))
            .expect("failed finding appropriate device");
        dbg!(&device_index, &device_info);

        Box::new(Self {
            instance,
            device_index,
            duplex_stream: None,
            input_stream: None,
            output_stream: None,
        })
    }

    fn make_duplex_stream(
        &mut self,
        processor: Arc<dyn Fn(&mut AudioContext)>,
    ) -> Result<Stream<NonBlocking, Duplex<f32, f32>>, portaudio::Error> {
        self.instance.open_non_blocking_stream(
            DuplexStreamSettings::new(
                StreamParameters::new(
                    self.device_index,
                    PREFERRED_CHANNELS.try_into().unwrap(),
                    false,
                    0.0,
                ),
                StreamParameters::new(
                    self.device_index,
                    PREFERRED_CHANNELS.try_into().unwrap(),
                    false,
                    0.0,
                ),
                PREFERRED_SAMPLE_RATE.into(),
                PREFERRED_BUFFER_SIZE,
            ),
            move |args: DuplexStreamCallbackArgs<f32, f32>| {
                processor(&mut AudioContext {
                    input: args.in_buffer,
                    output: args.out_buffer,
                });
                StreamCallbackResult::Continue
            },
        )
    }

    fn make_input_output_streams(
        &mut self,
        processor: Arc<dyn Fn(&mut AudioContext)>,
    ) -> Result<
        (
            Stream<NonBlocking, Input<f32>>,
            Stream<NonBlocking, Output<f32>>,
        ),
        portaudio::Error,
    > {
        let (mut input_producer, mut input_consumer) =
            RingBuf::new((PREFERRED_BUFFER_SIZE * 10).try_into().unwrap()).split();
        let mut output_stream_input_storage = vec![0.0; PREFERRED_BUFFER_SIZE.try_into().unwrap()];

        let input_stream = self.instance.open_non_blocking_stream(
            InputStreamSettings::new(
                StreamParameters::new(
                    self.device_index,
                    PREFERRED_CHANNELS.try_into().unwrap(),
                    false,
                    0.0,
                ),
                PREFERRED_SAMPLE_RATE.into(),
                PREFERRED_BUFFER_SIZE,
            ),
            move |args: InputStreamCallbackArgs<f32>| {
                for sample in args.buffer {
                    match input_producer.push(*sample) {
                        Ok(_) => {}
                        Err(_) => return StreamCallbackResult::Abort,
                    }
                }

                StreamCallbackResult::Continue
            },
        );

        let input_stream = match input_stream {
            Ok(input_stream) => input_stream,
            Err(error) => return Err(error),
        };

        let output_stream = self.instance.open_non_blocking_stream(
            OutputStreamSettings::new(
                StreamParameters::new(
                    self.device_index,
                    PREFERRED_CHANNELS.try_into().unwrap(),
                    false,
                    0.0,
                ),
                PREFERRED_SAMPLE_RATE.into(),
                PREFERRED_BUFFER_SIZE,
            ),
            move |args: OutputStreamCallbackArgs<f32>| {
                input_consumer.pop_slice(output_stream_input_storage.as_mut_slice());
                processor(&mut AudioContext {
                    input: output_stream_input_storage.as_slice(),
                    output: args.buffer,
                });
                StreamCallbackResult::Continue
            },
        );

        let output_stream = match output_stream {
            Ok(output_stream) => output_stream,
            Err(error) => return Err(error),
        };

        Ok((input_stream, output_stream))
    }

    fn cmp_hosts(x: &(HostApiIndex, HostApiInfo), _y: &(HostApiIndex, HostApiInfo)) -> Ordering {
        match x.1.host_type {
            #[cfg(any(
                target_os = "linux",
                target_os = "dragonfly",
                target_os = "freebsd",
                target_os = "netbsd"
            ))]
            HostApiTypeId::JACK => Ordering::Greater,
            _other => Ordering::Less,
        }
    }

    fn make_cmp_devices(
        host_index: HostApiIndex,
        _host_info: HostApiInfo,
    ) -> Box<dyn Fn(&(DeviceIndex, DeviceInfo), &(DeviceIndex, DeviceInfo)) -> Ordering> {
        Box::new(
            move |x: &(DeviceIndex, DeviceInfo), y: &(DeviceIndex, DeviceInfo)| -> Ordering {
                let x_device = &x.1;
                let y_device = &y.1;

                let x_ok = (x_device.host_api == host_index)
                    && (x_device.max_input_channels >= PREFERRED_CHANNELS.try_into().unwrap())
                    && (x_device.max_output_channels >= PREFERRED_CHANNELS.try_into().unwrap());
                let y_ok = (y_device.host_api == host_index)
                    && (y_device.max_input_channels >= PREFERRED_CHANNELS.try_into().unwrap())
                    && (y_device.max_output_channels >= PREFERRED_CHANNELS.try_into().unwrap());
                if x_ok && !y_ok {
                    return Ordering::Greater;
                }
                if y_ok && !x_ok {
                    return Ordering::Less;
                }

                Ordering::Equal
            },
        )
    }
}
