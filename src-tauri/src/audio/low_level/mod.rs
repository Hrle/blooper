pub use self::low_level_audio_backend_impl::*;
pub use self::traits::*;

mod config;
mod traits;

#[cfg(any(
    not(any(feature = "cpal", feature = "portaudio")),
    all(feature = "cpal", feature = "portaudio")
))]
compile_error!("either \"cpal\" or \"portaudio\" have to be enabled");

#[cfg(feature = "cpal")]
mod cpal;

#[cfg(feature = "portaudio")]
mod portaudio;

mod low_level_audio_backend_impl {
    #[cfg(feature = "cpal")]
    pub use super::cpal::*;

    #[cfg(feature = "portaudio")]
    pub use super::portaudio::*;
}
