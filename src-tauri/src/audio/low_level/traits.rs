use std::sync::Arc;

use super::config::{AudioConfig, Sample};

pub type AudioResult = Result<AudioConfig, (AudioConfig, String)>;

pub struct AudioContext<'a, 'b> {
    pub input: &'b [Sample],
    pub output: &'a mut [Sample],
}

pub trait LowLevelAudioBackendTrait: Send + Sync {
    fn process(&mut self, processor: Arc<dyn Fn(&mut AudioContext) + Send + Sync>) -> AudioResult;

    fn config(&mut self, config: AudioConfig) -> AudioResult;
}
