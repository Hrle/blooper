pub type Sample = f32;
pub type ChannelCount = u16;
pub type SampleRate = f32;
pub type BufferSize = u32;

pub static PREFERRED_CHANNELS: ChannelCount = 2;
pub static PREFERRED_SAMPLE_RATE: SampleRate = 48000.0;
pub static PREFERRED_BUFFER_SIZE: BufferSize = 128;

#[derive(Clone, Copy, Debug)]
pub struct AudioConfig {
    pub channels: ChannelCount,
    pub sample_rate: SampleRate,
    pub buffer_size: BufferSize,
}

pub static PREFERRED_AUDIO_CONFIG: AudioConfig = AudioConfig {
    channels: PREFERRED_CHANNELS,
    sample_rate: PREFERRED_SAMPLE_RATE,
    buffer_size: PREFERRED_BUFFER_SIZE,
};
