pub use crate::audio::low_level::AudioContext as Context;
pub use crate::state::value::Value as State;

pub trait RendererTrait: Send + Sync {
    fn render(&self, context: &mut Context, state: &State);
}
