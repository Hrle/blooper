pub mod traits;

use std::{cmp::min, sync::Arc};

pub use traits::{Context, RendererTrait, State};

#[derive(Debug, Clone, Copy)]
pub struct Renderer {}

impl Renderer {
    pub fn new() -> Arc<dyn RendererTrait> {
        Arc::new(Self {})
    }
}

impl RendererTrait for Renderer {
    fn render(&self, context: &mut Context, state: &State) {
        for index in 0..context.output.len() {
            context.output[index] = 0.0;
        }

        if state.volume.playing() {
            for track in state.tracks.iter() {
                if !track.volume.playing() {
                    continue;
                }

                for index in 0..min(context.input.len(), context.output.len()) {
                    context.output[index] =
                        state.volume.get() * track.volume.get() * context.input[index]
                            + context.output[index];
                }
            }
        }
    }
}
