pub mod r#const;
pub mod track;
pub mod value;
pub mod volume;

use std::sync::Arc;

use spinning_top::Spinlock;

use self::value::Value;

#[derive(Debug, Clone)]
pub struct State {
    value: Arc<Spinlock<Value>>,
}

impl State {
    pub fn new() -> Self {
        Self {
            value: Arc::new(Spinlock::new(Value::new())),
        }
    }

    pub fn with<T, R>(&self, todo: T) -> R
    where
        T: Fn(&mut Value) -> R,
    {
        {
            let mut value = self.value.lock();
            todo(&mut *value)
        }
    }
}
