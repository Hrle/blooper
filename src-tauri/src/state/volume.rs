use super::r#const::*;

#[derive(Clone, Copy, Debug)]
pub struct Volume {
    value: f32,
    previous: f32,
}

impl Volume {
    pub fn default() -> Self {
        Self::new(DEFAULT_VOLUME)
    }

    pub fn new(value: f32) -> Self {
        Self {
            value,
            previous: DEFAULT_VOLUME,
        }
    }

    pub fn get(&self) -> f32 {
        self.value
    }

    pub fn set(&mut self, value: f32) {
        self.value = value
    }

    pub fn playing(&self) -> bool {
        self.value != DEFAULT_VOLUME
    }

    pub fn mute(&mut self) {
        self.previous = self.value;
        self.value = DEFAULT_VOLUME
    }

    pub fn unmute(&mut self) {
        self.value = self.previous;
    }

    pub fn toggle(&mut self) -> bool {
        if self.playing() {
            self.mute();
            false
        } else {
            self.unmute();
            true
        }
    }
}

impl From<f32> for Volume {
    fn from(value: f32) -> Self {
        Self {
            value,
            previous: DEFAULT_VOLUME,
        }
    }
}

impl Into<f32> for Volume {
    fn into(self) -> f32 {
        self.value
    }
}
