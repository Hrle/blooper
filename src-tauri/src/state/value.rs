use super::r#const::*;
use super::track::Track;
use super::volume::Volume;

#[derive(Clone, Copy, Debug)]
pub struct Value {
    pub tracks: [Track; MAX_NUMBER_OF_TRACKS],
    pub volume: Volume,
}

impl Value {
    pub fn new() -> Self {
        Self {
            tracks: [Track::default(); MAX_NUMBER_OF_TRACKS],
            volume: Volume::default(),
        }
    }
}
