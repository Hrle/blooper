use super::volume::Volume;

#[derive(Clone, Copy, Debug)]
pub struct Track {
    pub volume: Volume,
}

impl Track {
    pub fn default() -> Self {
        Self {
            volume: Volume::default(),
        }
    }

    pub fn new(volume: f32) -> Self {
        Self {
            volume: Volume::new(volume),
        }
    }
}
