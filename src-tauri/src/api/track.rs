use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Debug, Clone, Copy)]
pub struct Track {
    pub playing: bool,
    pub volume: f32,
}
