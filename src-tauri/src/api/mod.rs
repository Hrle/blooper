mod track;

use crate::state::{r#const::MAX_NUMBER_OF_TRACKS, track::Track as StateTrack};
use track::Track;

pub type State<'a> = tauri::State<'a, crate::state::State>;

#[tauri::command]
pub fn get_playing(state: State) -> bool {
    state.with(|value| value.volume.playing())
}

#[tauri::command]
pub fn toggle_playing(state: State) -> bool {
    state.with(|value| value.volume.toggle())
}

#[tauri::command]
pub fn get_volume(state: State) -> f32 {
    state.with(|value| value.volume.into())
}

#[tauri::command]
pub fn set_volume(state: State, volume: f32) {
    state.with(|value| value.volume.set(volume))
}

#[tauri::command]
pub fn get_tracks(state: State) -> Vec<Track> {
    let tracks = state.with(|value| value.tracks);
    tracks
        .into_iter()
        .map(|track| Track {
            volume: track.volume.into(),
            playing: track.volume.playing(),
        })
        .collect()
}

#[tauri::command]
pub fn set_tracks(state: State, tracks: Vec<Track>) {
    let state_tracks: Vec<StateTrack> = tracks
        .iter()
        .map(|track| StateTrack::new(track.volume))
        .take(MAX_NUMBER_OF_TRACKS)
        .collect();
    state.with(|value| value.tracks.clone_from_slice(state_tracks.as_slice()))
}

#[tauri::command]
pub fn toggle_track_playing(state: State, track: usize) {
    state.with(|value| value.tracks[track].volume.toggle());
}

#[tauri::command]
pub fn set_track_volume(state: State, track: usize, volume: f32) {
    state.with(|value| value.tracks[track].volume.set(volume));
}
