#![cfg_attr(
    all(not(debug_assertions), target_os = "windows"),
    windows_subsystem = "windows"
)]

mod api;
mod audio;
mod state;

use std::sync::Arc;

use tauri::Manager;

use api::*;

use audio::low_level::{LowLevelAudioBackend, LowLevelAudioBackendTrait};
use audio::renderer::Renderer;
use state::State;

fn main() -> Result<(), String> {
    let state = State::new();
    let renderer = Renderer::new();

    let mut backend = LowLevelAudioBackend::new();
    let stream_renderer = renderer.clone();
    let stream_state = state.clone();
    match backend.process(Arc::new(move |stream_context| {
        let stream_state_value = stream_state.with(|value| value.clone());
        stream_renderer.render(stream_context, &stream_state_value)
    })) {
        Ok(_) => {}
        Err(error) => return Err(error.1),
    };

    let api_state = state.clone();
    tauri::Builder::default()
        .manage(api_state)
        .setup(|app| {
            #[cfg(debug_assertions)]
            {
                match app.get_window("main") {
                    Some(window) => {
                        window.open_devtools();
                        window.close_devtools();
                    }
                    None => {}
                };
            }
            Ok(())
        })
        .invoke_handler(tauri::generate_handler![
            get_playing,
            toggle_playing,
            get_volume,
            set_volume,
            get_tracks,
            set_tracks,
            set_track_volume,
            toggle_track_playing
        ])
        .run(tauri::generate_context!())
        .expect("error while running tauri application");

    Ok(())
}