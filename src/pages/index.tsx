import usePlayhead from '../lib/tauri/usePlayhead';
import { Button } from 'primereact/button';
import { Knob } from 'primereact/knob';
import { css } from '@emotion/react';
import { ThemeProvider } from '@emotion/react';

const theme = {
  colors: {
    primary: 'hotpink',
  },
};

function Blooper() {
  const {
    isPlaying,
    togglePlaying,
    volume,
    setVolume,
    tracks,
    setTracks,
    setTrackVolume,
    toggleTrackPlaying,
  } = usePlayhead();

  return (
    <ThemeProvider theme={theme}>
      <div css={css``}>
        <h1>Welcome to Blooper!</h1>

        <div>
          <Button onClick={togglePlaying}>Toggle play</Button>
        </div>

        <p>{isPlaying ? 'Playing' : 'Not playing'}</p>

        <div>
          <Knob
            value={volume}
            onChange={({ value: volume }) => setVolume(volume)}
            min={0}
            max={100}
          />
        </div>

        <div>
          <Button
            onClick={() =>
              setTracks([
                ...tracks,
                {
                  volume: 100,
                  playing: false,
                },
              ])
            }>
            Add track
          </Button>
          <Button onClick={() => setTracks(tracks.slice(0, -1))}>
            Remove track
          </Button>
        </div>

        <p>Volume: {volume}%</p>
        <div
          css={css`
            display: flex;
            direction: row;
          `}>
          {tracks.map((track, index) => (
            <div key={index}>
              <div>
                <Button onClick={() => toggleTrackPlaying(index)}>
                  Toggle play
                </Button>
              </div>

              <p>{track.playing ? 'Playing' : 'Not playing'}</p>

              <div>
                <Knob
                  value={track.volume}
                  onChange={({ value: volume }) =>
                    setTrackVolume(index, volume)
                  }
                  min={0}
                  max={100}
                />
              </div>
            </div>
          ))}
        </div>
      </div>
    </ThemeProvider>
  );
}

export default Blooper;
