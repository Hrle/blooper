import { useState } from 'react';
import { invoke } from '@tauri-apps/api/tauri';

export type Track = {
  playing: boolean;
  volume: number;
};

const usePlayhead = () => {
  const [isPlaying, setIsPlaying] = useState(true);
  const [volume, setVolume] = useState(100);
  const [tracks, setTracks] = useState<Track[]>([
    {
      playing: false,
      volume: 100,
    },
  ]);
  return {
    isPlaying,
    togglePlaying: () =>
      invoke('toggle_playing').catch(console.error).then(setIsPlaying),
    volume,
    setVolume: (volume: number) =>
      invoke('set_volume', { volume }).catch(console.error).then(setVolume),
    tracks,
    setTracks: (tracks: Track[]) =>
      invoke('set_tracks', { tracks }).catch(console.error).then(setTracks),
    toggleTrackPlaying: (track: number) =>
      invoke('toggle_track_playing', { track })
        .catch(console.error)
        .then((playing: boolean) => {
          const newTracks = [...tracks];
          newTracks[track].playing = playing;
          setTracks(newTracks);
        }),
    setTrackVolume: (track: number, volume: number) =>
      invoke('set_track_volume', { track, volume })
        .catch(console.error)
        .then((volume: number) => {
          const newTracks = [...tracks];
          newTracks[track].volume = volume;
          setTracks(newTracks);
        }),
  };
};

export default usePlayhead;
